package com.eanurag.github;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.FileUtils;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

public class GithubAPI {


    String endpoint;
    String username;
    String access_token;

    HttpURLConnection conn;


    private void initCredentials() {
        Properties prop = new Properties();
        try {
            System.setProperty("https.protocols", "TLSv1.2");
            prop.load(ClassLoader.getSystemClassLoader().getResourceAsStream("github.properties"));
            //check for system properties first, fall back to properties file
            this.endpoint = System.getProperties().getProperty("endpoint", prop.getProperty("endpoint"));
            this.username = System.getProperties().getProperty("username", prop.getProperty("username"));
            this.access_token = System.getProperties().getProperty("access_token", prop.getProperty("access_token"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private HttpURLConnection getConnection() {
        if (conn == null) {
            initCredentials();
            try {
                URL url = new URL(endpoint);
                conn = (HttpURLConnection) url.openConnection();
                String authHeader = username + ":" + access_token;
                byte[] message = authHeader.getBytes("UTF-8");
                String encoded = DatatypeConverter.printBase64Binary(message);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Authorization", "Basic " + encoded);
                conn.setDoOutput(true);
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        return conn;
    }

    public String createGist(String description, String content, String filename) {

        String gist_url = "";

        HttpURLConnection conn = getConnection();
        try {
            conn.setRequestMethod("POST");
            JsonObject parent = new JsonObject();
            parent.addProperty("description", description);
            parent.addProperty("public", "true");

            JsonObject gist = new JsonObject();
            gist.addProperty("content", content);

            JsonObject child = new JsonObject();
            child.add(filename, gist);

            parent.add("files", child);


            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            System.out.println(parent.toString());
            wr.write(parent.toString());
            wr.flush();
            wr.close();

            int rescode = conn.getResponseCode();

            if (rescode == HttpURLConnection.HTTP_CREATED) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                    gist_url = parseGistHtmlUrl(response.toString());
                }
                in.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return gist_url;

    }

    private String parseGistHtmlUrl(String response) {
        String html_url = null;
        JsonParser parser = new JsonParser();
        JsonObject result = (JsonObject) parser.parse(response);

        html_url = result.get("html_url").getAsString();
        return html_url;

    }


    public void getAllGists() {
        HttpURLConnection connection = getConnection();

        try {
            connection.setRequestMethod("GET");
            int rescode = connection.getResponseCode();
            System.out.println("Response from API: " + rescode);
            if (rescode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                saveAll(response.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void saveAll(String response) {
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(response).getAsJsonArray();
        int counter = 0;
        for (int i = 0; i <= array.size() - 1; i++) {
            JsonObject object = array.get(i).getAsJsonObject().get("files").getAsJsonObject();
            try {
                for (String key : object.keySet()) {
                    JsonObject innerObj = object.get(key).getAsJsonObject();
                    URL gistUrl = new URL(innerObj.get("raw_url").getAsString());
                    System.out.println(innerObj.get("raw_url").getAsString());


                    //create new filename based on the counter size
                    counter++;
                    File file = new File("Gist-" + counter);

                    FileUtils.copyURLToFile(gistUrl, file);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

}
